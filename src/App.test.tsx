import React from 'react';
import { render, screen } from '@testing-library/react';
import App from './App';

test('renders Life board', () => {
  render(<App />);
  const linkElement = screen.getByText(/Life board/i);
  expect(linkElement).toBeInTheDocument();
});
