import React, { useState } from 'react';
import './Board.css';
import { nextGeneration } from './helpers/helpers';

const rows = 20;
const cols = 20;
const allRows = Array.from({length : rows}).fill('') as string[];
const allCols = Array.from({length : cols}).fill('') as string[];

const Board : React.FC = () => {
    const [selected, setSelected] = useState<string[]>([]);

    const onClickCell = (row: number, col: number) => {
        const id = `${row},${col}`;
        if(selected.includes(id)){
            setSelected([...selected.filter((item) => item!==id)])
        }else{
            setSelected([...selected, `${row},${col}`])
        }
        
    }
    const clearHandler = () => {
        setSelected([])
    }

    const nextGenerationHandler = () => {
        const newSelected = nextGeneration(rows, cols, allRows, allCols, selected);
        setSelected(newSelected)
    }
    return (
        <div className='container'>
            <h1>Life board</h1>
            <div className='board'>
                {
                    allRows.map((_valR, indexR) => 
                        (<div className="row" key={indexR}>
                            {
                                allCols.map((_valC, indexC) => {
                                    return (<div key={`${indexR}-${indexC}`} className='cell' data-testid="cell" id={`cell-${indexR}-${indexC}`}
                                     style={{backgroundColor: selected.includes(`${indexR},${indexC}`) ? "#aaa" : "#eee"}}
                                      onClick = {() => { onClickCell(indexR, indexC) }}/>)
                                })
                            }
                        </div>)
                    )
                }
            </div>
            <div className='buttons'>
                <button onClick={nextGenerationHandler}>Next generation</button>
                <button onClick={clearHandler}>clear</button>
            </div>
        </div>
    )
}

export default Board;