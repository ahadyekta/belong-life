export const findNeighboars = (rows: number, cols:number, indexR: number, indexC: number) : string[] => {
    const neighboars:string[] =  [];
    [indexR-1, indexR, indexR+1].forEach((row) => {
        [indexC-1, indexC, indexC+1].forEach((col) => {
            if(row>-1 && row<rows && col>-1 && col<cols && !(col===indexC && row===indexR)){
                neighboars.push(`${row},${col}`)
            }
        })
    })
    return neighboars;
}

export const nextGeneration = (rows: number, cols:number, allRows:string[], allCols: string[], selected: string[]) => {
    const newSelected:string[] = [];
        allRows.forEach((_valR, indexR) => {
            allCols.forEach((_valC, indexC) => {
                const id = `${indexR},${indexC}`;
                const neighbours = findNeighboars(rows, cols, indexR, indexC);
                const liveNeighboars = neighbours.filter((id) => selected.includes(id));
                if(selected.includes(id)){
                    if(liveNeighboars.length === 2 || liveNeighboars.length === 3){
                        newSelected.push(id)
                    }
                }else{
                    if(liveNeighboars.length === 3){
                        newSelected.push(id)
                    }
                }
            });
        });

        return newSelected
}