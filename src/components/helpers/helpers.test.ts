import {findNeighboars, nextGeneration} from './helpers';

describe('helpers', () => {
    describe('findNeighboars', () => {
        it('should return 6 neighboars when it is not beside the edge', () => {
            const rows = 50;
            const cols = 50;
            const indexR = 25;
            const indexC = 25;
            const result = findNeighboars(rows, cols, indexR, indexC);
            expect(result).toEqual([
               "24,24",
               "24,25",
               "24,26",
               "25,24",
               "25,26",
               "26,24",
               "26,25",
               "26,26",
            ])
        });

        it('should return 3 neighboars when it is beside the edge', () => {
            const rows = 50;
            const cols = 50;
            const indexR = 0;
            const indexC = 0;
            const result = findNeighboars(rows, cols, indexR, indexC);
            expect(result).toEqual([
               "0,1",
               "1,0",
               "1,1",
            ])
        });
          
    })

    describe("nextGeneration", () => {
        it("should die if it only has 0 neighboar", () => {
            const rows = 50;
            const cols =50;
            const allRows= Array.from({length : rows}).fill('') as string[];
            const allCols = Array.from({length : rows}).fill('') as string[]; 
            const selected = ["0,0", "25,25"];
            const result = nextGeneration(rows, cols, allRows, allCols, selected);
            expect(result).toEqual([])
        })

        it("should die if it only has 1 neighboar", () => {
            const rows = 50;
            const cols =50;
            const allRows= Array.from({length : rows}).fill('') as string[];
            const allCols = Array.from({length : rows}).fill('') as string[]; 
            const selected = ["0,0", "0,1"];
            const result = nextGeneration(rows, cols, allRows, allCols, selected);
            expect(result).toEqual([])
        })

        it("should live if it has 2 neighboar and create a new cell when it has 3 neighboars", () => {
            const rows = 50;
            const cols =50;
            const allRows= Array.from({length : rows}).fill('') as string[];
            const allCols = Array.from({length : rows}).fill('') as string[]; 
            const selected = ["0,0", "0,1", "1,0"];
            const result = nextGeneration(rows, cols, allRows, allCols, selected);
            expect(result).toEqual(["0,0", "0,1", "1,0",  "1,1"])
        })
        it("should die if it has 4 neighboar", () => {
            const rows = 50;
            const cols =50;
            const allRows= Array.from({length : rows}).fill('') as string[];
            const allCols = Array.from({length : rows}).fill('') as string[]; 
            const selected = ["0,0", "0,1", "1,0", "1,1", "0,2"];
            const result = nextGeneration(rows, cols, allRows, allCols, selected);
            expect(result).toEqual(["0,0", "0,2", "1,0", "1,2"])
        })
    })
      
})

