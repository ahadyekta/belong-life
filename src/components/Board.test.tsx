import Board from './Board';
import { fireEvent, render, screen, waitFor } from '@testing-library/react';

describe("board", () => {
    it('should render all the cells', () => {
       const { getAllByTestId }= render(<Board />);
       expect(getAllByTestId('cell').length).toEqual(400);
    })
    it('should select/unselect when we click on any cell', () => {
        const { container }= render(<Board />);
        const cell = container.querySelector('#cell-1-1')!;
        expect(container.querySelector('#cell-1-1')).toHaveStyle('background-color: #eee')
        fireEvent.click(cell);
        waitFor(() => {
            expect(container.querySelector('#cell-1-1')).toHaveStyle('background-color: #ccc')
        })
        fireEvent.click(cell);
        waitFor(() => {
            expect(container.querySelector('#cell-1-1')).toHaveStyle('background-color: #eee')
        })
     })
})